# Anleitung: Raspberry Pi als Dokumentenserver

### erstellt Christian Pfliegel

### Mit Raspberry Pi verbinden und Wordpress aufrufen

1. Raspberry Pi einstecken und ca. 1 Minute warten
2. WLan "RPi3" suchen und verbinden
3. Passwort: **Schermen138!**
4. Browser öffnen und 192.168.4.1./wordpress/wp-admin/ in die Adresszeile eingeben
5. Folgendes Fenster öffnet sich: 

![Login](images/login.png)

6. Benutzer: christian, Passwort: pinwap-2vetqA-xowsor
7. "Anmelden" klicken


### Dateien in Wordpress hochladen
1. In der linken Spalte auf Medien klicken
2. Oben neben "Mediathek" auf "Datei hinzufügen" klicken und die hochzuladende Datei auf der Festplatte auswählen (alle Dateien bis 2GB sind möglich)
3. Warten bis die Datei hochgeladen ist (sichtbar in der unteren Hälfte des Fensters)
4. Wenn alles geklappt hat sollte es folgendermaßen aussehen: 

![Wordpress](images/wordpress.png)

5. Auf die hochgeladene Datei klicken
6. Auf der rechten Seite im Fenster unter "Link kopieren" findet sich der Link zur Datei
7. Dieser Link kann den Teilnehmer:innen gegeben werden. Sie müssen sich mit dem Raspberry Pi verbinden (Wlan: RPi3, Passwort: Schermene138!) und die Link-Adresse im Browser eingeben


### Alternative: QR-Code
1. Das Eingeben der langen Adresse ist recht fehleranfällig, daher ist es empfehlenswert, einen QR-Code anzulegen: 
2. https://www.qrcode-generator.de/ aufrufen
3. Link von oben (ähnlich wie dieser: http://192.168.4.1/wp-content/uploads/2019/11/PlakatDruck-2.pdf) bei Webseite-URL einfügen
4. QR-Code wird automatisch erstellt
5. QR-Code mit "Download jpg" herunterladen und ausdrucken
6. Teilnehmer:innen können sich nun mit dem Raspberry Pi verbinden und **danach** den Code scannen, dann wird die Datei direkt auf dem Handy angezeigt und kann gespeichert werden





